<?
require_once './core.php';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="./style.css">
</head>
<body>
<div class="main-container container">
    <? require 'views/header.php'; ?>
    <? require 'views/filter.php'; ?>
    <? require 'views/tasks.php'; ?>
    <? require 'views/pagination.php'; ?>
    <? require 'views/modal.php'; ?>
    <? require 'views/alert.php'; ?>

    <form class="d-none">
        <button id="hidden_params" type="submit" class="btn btn-primary">Submit</button>

        <input id="current_page" type="hidden" name="page_number" value="<?=@$data['page_number'] + 1?>">
        <input id="current_field" type="hidden" name="field" value="<?=@$data['field']?>">
        <input id="current_sort" type="hidden" name="order" value="<?=$data['order']?>">
    </form>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script>

    function next_page(page_number){
        $('#current_page').val(page_number - 1);
        $('#hidden_params').trigger('click');
    }

    $('.page_number').on('click', e => {

        let nex_page = $(e.currentTarget).text();

        next_page (nex_page);
    })

    $('.page_previous').on('click', e => {
        let current = $('#current_page').val();

        if(current > 1){
            --current
            next_page (current);
        }
    })

    $('.page_next').on('click', e => {
        let current = $('#current_page').val();
        let final_page = $('.pagination li');
        final_page = $(final_page[final_page.length - 2]).text();

        if(current < final_page){
            ++current
            console.log(current)
            next_page (current);
        }
    })

    function set_modal (params){
        let modal = $('.modal-form');

        if(params){
            modal.find('#create-name').val(params['name'])
            modal.find('#create-email').val(params['email'])
            modal.find('#message-text').text(params['message'])
            modal.find('#create-status').attr('checked', !!Number.parseInt(params['status']))
            modal.find('#task-id').attr('value', params['id'])
            modal.find('#task-action').attr('value', 'edit')
        } else {
            modal.find('#create-name').val('')
            modal.find('#create-email').val('')
            modal.find('#message-text').text('')
            modal.find('#create-status').attr('checked', false)
            modal.find('#task-id').attr('value', '-1')
            modal.find('#task-action').attr('value', 'create')
        }

        $('#exampleModal').modal('show')
    }

    $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        if(button.attr('data-toggle') === 'modal')
            set_modal();
    })

    $('.edit-task').on('click', e => {

        let card = $(e.currentTarget).parents('.card')

        let params = [];
        params['name'] = card.find('.card-title').text();
        params['email'] = card.find('.card-email').text();
        params['message'] = card.find('.card-message').text();
        params['status'] = card.find('.card-status').attr('data-status');
        params['id'] = card.attr('data-id');

        set_modal(params);
    })

    if($('#alertModal').attr('data-show') !== '0'){
        $('#alertModal').modal('show');
        setTimeout(() => $('#alertModal').modal('hide'), 3000);
    }

</script>

</body>
</html>
