<form class="filter">
    <div class="form-row align-items-center">
        <div class="col-auto my-1">
            <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect">Preference</label>
            <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="field">
                <option value="-1" selected>Сортировать по...</option>
                <option value="user_name" <?=$data['field'] === 'user_name' ? 'selected' :'' ?>>Имя пользователя</option>
                <option value="email" <?=$data['field'] === 'email' ? 'selected' :'' ?>>Email</option>
                <option value="status" <?=$data['field'] === 'status' ? 'selected' :'' ?>>Статус</option>
            </select>
        </div>
        <div class="col-auto my-1">
            <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect">Preference</label>
            <select class="custom-select mr-sm-2" id="inlineFormCustomSelect" name="order">
                <option value="-1" selected>Сортировать по...</option>
                <option value="asc" <?=$data['order'] === 'asc' ? 'selected' :'' ?>>Возрастанию</option>
                <option value="desc" <?=$data['order'] === 'desc' ? 'selected' :'' ?>>Убыванию</option>
            </select>
        </div>
        <div class="col-auto my-1">
            <button id="filter" type="submit" class="btn btn-primary">Сортировать</button>
        </div>
    </div>
</form>
