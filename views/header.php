<nav class="navbar navbar-expand-lg navbar-light bg-light d-flex justify-content-between">

    <button type="button" class="btn btn-primary"  data-toggle="modal" data-target="#exampleModal">
        Новая задача
    </button>


    <form class="login-form <?= $_SESSION['isAdmin'] ? 'd-none' : 'd-flex'?>">
        <input name="login" type="text" class="form-control" placeholder="Login" required="required">
        <input name="password" type="password" class="form-control" placeholder="Password" required="required">
        <input name="action" type="hidden" class="form-control" value="login">
        <div class="col-auto my-1">
            <button id="filter" type="submit" class="btn btn-primary">Войти</button>
        </div>
    </form>
    <form class="logout-form <?= !$_SESSION['isAdmin'] ? 'd-none' : 'd-flex'?>">
        <input name="action" type="hidden" class="form-control" value="logout">
        <div>Admin</div>
        <div class="col-auto my-1">
            <button id="filter" type="submit" class="btn btn-primary">Выйти</button>
        </div>
    </form>

</nav>
