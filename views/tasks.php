<ul class="items-list">
    <?php foreach ($tasks as $task): ?>

        <div data-id="<?=$task['id']?>" class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title"><?=$task['user_name'] ?></h5>
                <p class="card-email"><?=$task['email'] ?></p>
                <p class="card-message"><?=$task['text'] ?></p>

                <?php if($task['status']): ?>
                    <p class="card-status" data-status="<?=$task['status']?>">
                        <span class="badge badge-pill badge-success">
                            Success
                        </span>
                    </p>
                <?php else: ?>
                    <p class="card-status" data-status="<?=$task['status']?>">
                        <span class="badge badge-pill badge-warning">
                            Active
                        </span>
                    </p>
                <?php endif; ?>

                <div class="card-marks">
                    <?php if($_SESSION['isAdmin']): ?>
                        <a class="edit-task" href="#" class="badge badge-light">Редактировать</a>
                    <?php endif; ?>

                    <?php if($task['edited']): ?>
                        <span class="badge badge-pill badge-info">Изменено администратором</span>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    <?php endforeach; ?>
</ul> 
