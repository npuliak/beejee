<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">

        <div class="modal-content">
            <form class="modal-form">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="create-name" class="col-sm-3 col-form-label col-form-label-sm">User name</label>
                        <div class="col-sm-9">
                            <input name="name" type="text" class="form-control form-control-sm" id="create-name" placeholder="User name" required="required">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="create-email" class="col-sm-3 col-form-label col-form-label-sm">Email</label>
                        <div class="col-sm-9">
                            <input name="email" type="email" class="form-control form-control-sm" id="create-email" placeholder="Email" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Message:</label>
                        <textarea name="text" class="form-control" id="message-text"></textarea>
                    </div>

                    <div class="custom-control custom-checkbox">
                        <input name="status" type="checkbox" class="custom-control-input" id="create-status">
                        <label class="custom-control-label" for="create-status">Завершена</label>
                    </div>

                </div>

                <input id="task-action" name="action" type="hidden" value="create">
                <input id="task-id" name="task-id" type="hidden" value="-1">

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Send message</button>
                </div>
            </form>
        </div>
    </div>
</div>
