<nav aria-label=" Page navigation example">
    <ul class="pagination">
        <li class="page-item">
            <a class="page_previous page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
            </a>
        </li>

        <?php for ($i = 1; $i <= $page_count; ++$i): ?>

            <?php if($i === $page_number + 1): ?>
                <li class="page-item active"><a class="page-link" href="#"><?=$i ?></a></li>
            <?php else: ?>
                <li class="page_number page-item"><a class="page-link" href="#"><?=$i ?></a></li>
            <?php endif; ?>

        <?php endfor; ?>

        <li class="page_next page-item">
            <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
            </a>
        </li>
    </ul>
</nav>
