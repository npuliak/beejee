
<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel" aria-hidden="true" data-show="<?=$show_alert ?>">
    <div class="modal-dialog" role="document">

        <div class="alert alert-success" role="alert">
            Задача успешно сохранена!
        </div>
    </div>
</div>
