<?php
require_once 'models/local_config.php';
require_once 'models/user.php';
require_once 'models/task.php';

$page_number = 1;
$page_limit = 3;
$page_count = 0;
$show_alert = 0 ;

$data = $_GET;

session_start();

try{
    $task_model = new Task(
        DB_HOST,
        DB_NAME,
        DB_USER,
        DB_PASSWORD,
        DB_CHARSET);

    $page_number = $data['page_number'];

    $offset = $page_number * $page_limit;

    $page_count = ceil($task_model->count() / $page_limit);

    if(isset($data['field']) && $data['field'] !== '-1')
    {
        if($data['order'] !== 'asc')
            $tasks = $task_model->find($offset, $data['field'], 'ASC');
        else
            $tasks = $task_model->find($offset, $data['field'], 'DESC');
    }
    else
    {
        $tasks = $task_model->find($offset);
    }
    if($data['action'] === 'login')
    {
        $user_model = new User(
            DB_HOST,
            DB_NAME,
            DB_USER,
            DB_PASSWORD,
            DB_CHARSET);

        $user = $user_model->find($data['login'], $data['password']);

        if(count($user)) {
            $_SESSION['isAdmin'] = true;
        } else {
            $_SESSION['isAdmin'] = false;
        }
    }
    if($data['action'] === 'logout')
    {
        $_SESSION['isAdmin'] = false;
    }
    if($data['action'] === 'create')
    {
        $params['name'] = $data['name'];
        $params['text'] = $data['text'];
        $params['email'] = $data['email'];
        $params['status'] = $data['status'];

        $user = $task_model->insert($params);

        $show_alert = 1;
    }
    if($data['action'] === 'edit')
    {
        if($_SESSION['isAdmin'])
        {
            $params['name'] = $data['name'];
            $params['text'] = $data['text'];
            $params['email'] = $data['email'];
            $params['status'] = $data['status'];
            $params['id'] = $data['task-id'];

            $user = $task_model->update($params);
        }
    }

} catch (Exception $e) {
    header('./index.php');
    echo $e->getMessage();
}
