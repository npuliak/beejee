<?php


class User
{
    private $pdo;

    public function __construct($host, $db, $user, $password, $charset = '')
    {
        if($charset)
            $charset = ";charset=$charset";
        $dsn = "mysql:host=$host;dbname=$db$charset";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $this->pdo = new PDO($dsn, $user, $password, $opt);

    }

    public function find ($name, $password)
    {
        $sql = "SELECT * FROM `user` 
                WHERE `name` = '$name' 
                AND `password` = '$password'";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}
