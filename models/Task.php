<?php


class Task
{
    private $pdo;

    public function __construct($host, $db, $user, $password, $charset = '')
    {
        if($charset)
            $charset = ";charset=$charset";
        $dsn = "mysql:host=$host;dbname=$db$charset";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $this->pdo = new PDO($dsn, $user, $password, $opt);

    }

    public function insert ($params)
    {
        $sql = "INSERT INTO `tasks`(`user_name`, `text`, `status`, `email`) 
                VALUES (
                '" . $params['name'] . "',
                '" . $params['text'] . "',
                " . ($params['status'] ? 1 : 0) . ",
                '" . $params['email'] . "'
                )";

        return $this->pdo->exec($sql);
    }

    public function update ($params)
    {
        $sql = "
            UPDATE `tasks` 
            SET
            `user_name`='" . $params['name'] . "',
            `text`='" . $params['text'] . "',
            `status`=" . ($params['status'] ? 1 : 0) . ",
            `email`='" . $params['email'] . "',
            `edited`= 1
            WHERE `id`=" . $params['id'];

        return $this->pdo->exec($sql);
    }

    public function find ($offset, $order_by = '', $type = '')
    {
        $sql = "SELECT * FROM `tasks` ";
        if($order_by)
            $sql .= "ORDER BY `$order_by` $type ";
        $sql .= "LIMIT 3 OFFSET $offset";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function count ()
    {
        $sql = "SELECT COUNT(*) FROM `tasks`";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll()[0]["COUNT(*)"];
    }
}
